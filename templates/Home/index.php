
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                <table class="table">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data as $f ): ?>
                        <tr>
                            <td><?php echo $f ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</body>
</html>
